<!-- @format -->

# Watheia Labs, LLC

**Pros of using a business plan template:** A good business plan template can help you get
your thoughts organized. It can provide a guideline so you’re not stuck looking at a blank
page trying to figure out where to start. Plus, it shows you the general layout of a
standard business plan so you know what goes where and that you're not leaving out anything.

A great business plan template will also provide instructions for each step of your plan and
show you what an investor-ready and SBA-approved business plan should look like.

**Cons of using a business plan template:** While there are benefits to using a business
plan template, depending on your situation it may not be the best way to complete your plan.
There is still going to be a lot of work involved. For instance, not only do you have to
complete the financial spreadsheets, but you have to do the math yourself.

You’ll also have to know enough about the process to be sure you’re getting the numbers in
the right place. So, if you don’t already know what you’re doing with the numbers, the
process of writing your business plan may not be that much easier with a template.

Finally, merging data from
[Excel spreadsheets](https://www.thebalancesmb.com/excel-spreadsheets-for-small-business-accounting-4163594)
into your Word document is harder than it looks. It’s not easy to keep everything completely
up-to-date as you make changes to your numbers, and integrating the right charts and graphs
into your business plan is harder than it looks.

<span id="business-plan-templates-make-sense"
class="heading-toc"></span>

## <span class="mntl-sc-block-heading__text"> Business Plan Templates Make Sense </span>

However, if you’re new to
[business planning](https://www.thebalancesmb.com/small-business-makeover-create-a-business-action-plan-2948290)
and just want to get a sense of what a plan looks like and want to get the process started
quickly and cheaply, then downloading a free template is the best way to get started.

<span id="do-i-need-a-simple-or-detailed-plan"
class="heading-toc"></span>

## <span class="mntl-sc-block-heading__text"> Do I Need a Simple or Detailed Plan? </span>

A corporate business plan for a large organization can be hundreds of pages long. However,
for a small business, it's best to keep the plan short and concise, especially if you intend
to submit it to bankers or investors. Capping your plan at 30 pages should be sufficient
unless you need to include photos of products, equipment,
[logos](https://www.thebalancesmb.com/creating-a-great-business-logo-and-tag-line-2948333),
business premises or site plans, etc. Potential money lenders
and [investors](https://www.thebalancesmb.com/the-7-things-angel-investors-are-looking-for-2948104) want
solid research and analysis, not long, wordy descriptions.

<span id="how-to-use-the-template" class="heading-toc"></span>

## <span class="mntl-sc-block-heading__text"> How to Use the Template </span>

The business plan template below is broken into sections as described in the table of
contents. Each section of the template can be copied into a Word, Excel or similar office
document by selecting the text and using copy/paste. If using Windows, outline the text to
be selected with the mouse and hit CTRL+C to copy and CTRL+V to paste.

Once you complete your simple business plan be sure to format it attractively, print it and
get it professionally bound. You want your business plan to convey the best possible
physical impression; make it something people are going to want to pick up and look at.

<span id="title-page" class="heading-toc"></span>

## <span class="mntl-sc-block-heading__text"> Title Page </span>

Enter your business information including the legal name, address, etc. If you already have
a business logo you can add it at the top or bottom of the title page.

- **Business Plan for "Business Name"**
  - Date
  - Business address
  - Phone
  - Email
  - Website
  - If addressing to a company or individual include:  
    Presented to: "Name"
  - Company or Financial Institution

<span id="table-of-contents" class="heading-toc"></span>

## <span class="mntl-sc-block-heading__text"> Table of Contents </span>

1.  Executive Summary..............................................Page \#
2.  Business/Industry Overview.................................Page \#
3.  Market Analysis and Competition........................Page \#
4.  Sales and Marketing Plan....................................Page \#
5.  Ownership and Management Plan.......................Page \#
6.  Operating Plan.....................................................Page \#
7.  Financial Plan.......................................................Page \#
8.  Appendices and Exhibits......................................Page \#

<span id="section-1-executive-summary" class="heading-toc"></span>

## <span class="mntl-sc-block-heading__text"> Section 1: Executive Summary </span>

The [executive summary](https://www.thebalancesmb.com/business-plan-executive-summary-example-2948007)
goes near the beginning of the plan but is written last. It provides a short, concise, and
optimistic overview of your business that captures the reader's attention and creates a need
to learn more. The executive summary should be no more than two pages long, with brief
summaries of other sections of the plan.

- Describe
  your [mission](https://www.thebalancesmb.com/how-to-write-a-mission-statement-2948001) - what
  is the need for your new business?
- Introduce your company and the management and ownership.
- Describe your main product and service offerings.
- Briefly describe the customer base you will be targeting and how your business will serve
  those customers.
- Summarize the competition and how you will get market share (i.e., what is your
  competitive advantage?)
- Briefly outline your financial projections for the first few years of operation.
- Describe
  your [start-up financing](https://www.thebalancesmb.com/how-do-i-find-small-business-start-up-money-2948600) requirements
  (if applicable).

An overview of the industry and how your business will compete in the sector. If you need
guidance,
a [Business Plan Example of the Industry Overview](https://www.thebalancesmb.com/business-plan-example-of-the-industry-overview-2948009) will
prove useful.

<span id="section-2-businessindustry-overview"
class="heading-toc"></span>

## <span class="mntl-sc-block-heading__text"> Section 2: Business/Industry Overview </span>

- Describe the overall nature of the industry, including sales and other statistics.
  Include trends and demographics, and economic, cultural, and governmental influences.
- Describe your business and how it fits into the industry.
- Describe the existing competition.
- Describe what [area(s) of the market you will target](https://www.thebalancesmb.com/define-your-customer-before-marketing-2947197) and
  what unique, improved or lower cost services you will offer.

<span id="section-3-market-analysis-andcompetition"
class="heading-toc"></span>

## <span class="mntl-sc-block-heading__text"> Section 3: Market Analysis and Competition </span>

In this section, you need to demonstrate that you have thoroughly analyzed the target market
and that there is enough demand for your product or service to make your business viable.
The competitive analysis includes an assessment of your competition and how your business
will compete in the sector. You can turn
to [How to Write the Competitor Analysis Section of the Business Plan](https://www.thebalancesmb.com/how-to-write-the-competitive-analysis-section-of-the-business-plan-2947025) for
help. The [target market](https://www.thebalancesmb.com/target-marketing-2948355)
description and competitive analysis portions can be two separate sections in the plan or
combined as shown:

- Define the target market(s) for your product or service in your geographic locale.
- Describe the need for your products or services.
- Estimate the overall size of the market and the units of your product or service the
  target market might buy, potential repeat purchase volume, and how the market might be
  affected by economic or demographic changes.
- Estimate the volume and value of your sales in comparison with
  any [existing competitors](https://www.thebalancesmb.com/how-to-scope-out-competition-2948366).
  It helps to summarize the results in table form as in the following example which
  demonstrates that there is a gap in the high-quality sector of the market that your
  business intends to target.
- Describe any helpful barriers to entry that may protect your business from competition,
  such as access to capital, technology, regulations, employee skill sets, location, etc.

<table class="mntl-sc-block-table__table">
<tbody data-check="-1">
<tr class="odd">
<td><strong>Business</strong></td>
<td><strong>Competitor A</strong></td>
<td><strong>Competitor B</strong></td>
<td><strong>Your Business</strong></td>
</tr>
<tr class="even">
<td>Est. Annual Revenue</td>
<td>$1,000,000</td>
<td>$600,000</td>
<td>$500,000</td>
</tr>
<tr class="odd">
<td>Employees</td>
<td>20</td>
<td>10</td>
<td>5</td>
</tr>
<tr class="even">
<td>Price</td>
<td>Average</td>
<td>High</td>
<td>High</td>
</tr>
<tr class="odd">
<td>Quality</td>
<td>Low</td>
<td>Average</td>
<td>High</td>
</tr>
</tbody>
</table>

<span id="section-4-sales-and-marketing-plan"
class="heading-toc"></span>

## <span class="mntl-sc-block-heading__text"> Section 4: Sales and Marketing Plan </span>

A description of how you intend to entice customers to buy your product(s) or service(s),
including advertising/promotion,
[pricing strategy](https://www.thebalancesmb.com/retail-pricing-strategies-2890279), sales
and distribution, and post-sales support if applicable.

**Product or Service Offerings**

- Describe your product or service, how it benefits the customer, and what sets it apart
  from competitor offerings (i.e., what is
  your [Unique Selling Proposition](https://www.thebalancesmb.com/unique-selling-proposition-2948356)?).

**Pricing Strategy**

- Describe how you intend to price your product or service. Pricing has to be competitive to
  attract customers but high enough to cover costs and generate a profit. Pricing can be
  based on markup from cost, value to the buyer, or in comparison with similar
  products/services in the
  marketplace. [Breakeven analysis](https://www.thebalancesmb.com/breakeven-analysis-2947266) can
  help determine sales and pricing for profitability. You'll also want to take a look
  at [Retail Pricing Strategies to Increase Profitability](https://www.thebalancesmb.com/retail-pricing-strategies-2890279).

**Sales and Distribution**

- Describe how you will distribute your products to the customer (if applicable). Will you
  be selling wholesale or retail? What type of packaging will be required? How will the
  product(s) be shipped? What methods will be used for payment?

**Advertising and Promotion**

- List the different media you will use to get your message to customers
  (e.g., [business website](https://www.thebalancesmb.com/how-to-create-a-small-business-website-that-works-2947218),
  email, [social media](https://www.thebalancesmb.com/how-to-create-a-social-media-plan-2948529),
  traditional media like newspapers, etc.). Will you
  use [sales promotional](https://www.thebalancesmb.com/business-promotion-definition-2947189) methods
  such as free samples, product demonstrations, etc.?
- What marketing materials you'll use such
  as [business cards](https://www.thebalancesmb.com/business-cards-2947923),
  flyers, [brochures](https://www.thebalancesmb.com/brochures-5-tips-for-a-great-brochure-1794595),
  etc. What about product launches
  and [tradeshows](https://www.thebalancesmb.com/before-you-attend-that-trade-show-2947181)?
  Include an approximate budget
  for [advertising](https://www.thebalancesmb.com/small-business-advertising-ideas-2947892) and
  promotion.

<span id="section-5-ownership-and-management-plan"
class="heading-toc"></span>

## <span class="mntl-sc-block-heading__text"> Section 5: Ownership and Management Plan </span>

This section describes the legal structure, ownership, and (if applicable) the management,
and staffing requirements of your business.

**Ownership Structure**

- Describe the legal structure of your
  company (e.g., corporation, partnership, [Limited Liability Company](https://www.thebalancesmb.com/what-is-a-limited-liability-company-llc-398316),
  or [sole proprietorship](https://www.thebalancesmb.com/sole-proprietorship-2947269)). List
  ownership percentages if applicable. If the business is a sole proprietorship this is the
  only section required.

**Management Team**

- Describe managers and their roles, key employee positions, and how each will be
  compensated. Include brief resumés.

**External Resources and Services**

- List any external professional resources required, such
  as [accountants](https://www.thebalancesmb.com/how-to-find-a-good-accountant-for-your-small-business-2947890),
  lawyers, [consultants](https://www.thebalancesmb.com/small-business-consulting-4140676),
  etc.

**Human Resources**

- List the type and number
  of [employees or contractors](https://www.thebalancesmb.com/hiring-employees-vs-hiring-contractors-the-pros-and-cons-2948201) you
  will need and an estimate of the salary and benefit costs of each.

**[Advisory Board](https://www.thebalancesmb.com/harness-the-power-of-an-advisory-board-2948319) (if
required)**

- Include
  an [advisory board](https://www.thebalancesmb.com/harness-the-power-of-an-advisory-board-2948319) as
  a supplemental management resource (if applicable).

<span id="section-6-operating-plan" class="heading-toc"></span>

## <span class="mntl-sc-block-heading__text"> Section 6: Operating Plan </span>

The operating plan outlines the physical requirements of your business, such as office,
warehouse, retail space, equipment, inventory and supplies, labor, etc. For a one-person,
[home-based](https://www.thebalancesmb.com/home-based-business-2948188) consulting business
the operating plan will be short and simple, but for a business such as a restaurant or a
manufacturer that requires custom facilities, supply chains, specialized equipment, and
[multiple employees](https://www.thebalancesmb.com/top-ways-to-attract-quality-employees-2948197),
the operating plan needs to be very detailed.

**Development (if applicable)**

- Explain what you have done to date in terms of identifying possible locations, sources of
  equipment, [supply chains](https://www.thebalancesmb.com/non-supply-chain-manager-2221309),
  etc. Describe your production workflow.

**Production**

- For manufacturing, explain how long it takes to produce a unit and when you'll be able to
  start producing your product or service. Include factors that may affect the time frame of
  production and how you'll deal with potential problems such as rush orders.

**Facilities**

- Describe the physical location of the business including location, land, and building
  requirements. Include square footage estimates with room for expansion if expected.
  Include
  the [mortgage or leasing costs](https://www.thebalancesmb.com/should-your-business-lease-or-purchase-commercial-space-2948304).
  Also include estimates of expected maintenance, utilities, and
  related [overhead costs](https://www.thebalancesmb.com/reduce-business-overhead-costs-2948305).
  Include zoning approvals and other permissions necessary to operate your business.

**Staffing**

- Outline expected staffing needs and the main duties of staff members, especially the key
  employees. Describe how the employees will be sourced and the employment relationship
  (i.e., [contract](https://www.thebalancesmb.com/the-advantages-of-being-a-contractor-2948558),
  full-time, part-time, etc.). Detail any employee training needed and how it will be
  provided.

**Equipment**

- Include a list of any specialized equipment needed. Include the cost and whether it will
  be leased or purchased and the sources.

**Supplies**

- If your business is manufacturing, retail, food services, etc. include a description of
  the materials needed and how you will reliably source them. Give descriptions of major
  suppliers if needed. Describe how you will manage inventory.

<span id="section-7-financial-plan" class="heading-toc"></span>

## <span class="mntl-sc-block-heading__text"> Section 7: Financial Plan </span>

The financial plan section is the most important section of the business plan, especially if
you need debt financing or want to
[attract investors](https://www.thebalancesmb.com/prepare-an-investor-ready-business-plan-2947200).
The financial plan has to demonstrate that your
[business will grow](https://www.thebalancesmb.com/top-ways-of-growing-your-business-2948140)
and be
[profitable](https://www.thebalancesmb.com/increase-your-service-business-profits-2948142).
To do this, you will need to create projected income statements,
[cash flow](https://www.thebalancesmb.com/cash-management-is-important-for-your-small-business-393118)
statements, and balance sheets. For a new business, these are forecasts. A good rule of
thumb is to underestimate revenues and overestimate expenses.

**Income Statements**

- The income statement shows your projected revenues, expenses and profit. Do this on a
  monthly basis for at least the first year for a startup business.

**Cash Flow Projections**

- The cash flow projection shows your monthly anticipated cash revenues and disbursements
  for expenses. It is important for demonstrating that you can manage
  your [cash flow](https://www.thebalancesmb.com/cash-flow-analysis-for-small-business-owners-2947137) and
  will be a good credit risk.

**Balance Sheet**

- The [balance sheet](https://www.thebalancesmb.com/balance-sheet-definition-2946947) is a
  snapshot summary of the [assets](https://www.thebalancesmb.com/assets-definition-2947887),
  liabilities, and equity of your business at a particular point in time. For a startup,
  this would be on the day the business opens. Note that a new business will have no
  accounts receivable entries on the balance sheet. Note also that the Balance Sheet is much
  simpler for unincorporated businesses without employees. Income tax, pensions, medical,
  etc. are only applicable to incorporated businesses, as are earnings/retained earnings.

**Breakeven Analysis**

- Including a breakeven analysis will demonstrate to financiers or investors what level of
  sales you need to achieve to make a profit.

<span id="section8-appendices-and-exhibits" class="heading-toc"></span>

## <span class="mntl-sc-block-heading__text"> Section 8: Appendices and Exhibits </span>

The appendices and exhibits section contains any detailed information needed to support
other sections of the plan.

**Possible Appendix/Exhibit Items**

- Credit histories for the business owners
- Detailed market research and analysis of competitors
- Resumés of the owners and key employees
- Information about your industry
- Information about your products/services
- Site/building/office plans
- Copies of mortgage documents, equipment leases, etc. (or quotes on these)
- Marketing brochures and other materials
- References from business colleagues
- Links to
  your [business website](https://www.thebalancesmb.com/reasons-small-business-website-2948414)
- Any other supporting material that may impress potential lenders or investors if you are
  looking for financing​

- <span class="share-link share-link-facebook"
  data-href="https://www.facebook.com/dialog/share?app_id=159081794784966&amp;display=popup&amp;href=https%3A%2F%2Fwww.thebalancesmb.com%2Fentrepreneur-simple-business-plan-template-4126711%3Futm_source%3Dfacebook%26utm_medium%3Dsocial%26utm_campaign%3Dshareurlbuttons&amp;redirect_uri=https%3A%2F%2Fwww.thebalancesmb.com%2FfacebookShareRedirect.htm"
  network="facebook" click-tracked="true" title="Share on Facebook">
  <img src="data:image/svg+xml;base64,PHN2ZyBjbGFzcz0iaWNvbiBpY29uLWZhY2Vib29rICI+Cjx1c2UgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhsaW5rOmhyZWY9IiNpY29uLWZhY2Vib29rIj48L3VzZT4KPC9zdmc+" class="icon icon-facebook" />
  Share </span>
- <span class="share-link share-link-twitter"
  data-href="https://twitter.com/intent/tweet?url=https%3A%2F%2Fwww.thebalancesmb.com%2Fentrepreneur-simple-business-plan-template-4126711%3Futm_source%3Dtwitter%26utm_medium%3Dsocial%26utm_campaign%3Dshareurlbuttons&amp;via=thebalance&amp;text=Use+This+Template+to+Write+a+Simple+Business+Plan"
  network="twitter" click-tracked="true" title="Share on Twitter">
  <img src="data:image/svg+xml;base64,PHN2ZyBjbGFzcz0iaWNvbiBpY29uLXR3aXR0ZXIgIj4KPHVzZSB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeGxpbms6aHJlZj0iI2ljb24tdHdpdHRlciI+PC91c2U+Cjwvc3ZnPg==" class="icon icon-twitter" />
  Tweet </span>
- <span class="share-link share-link-linkedin"
  data-href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https%3A%2F%2Fwww.thebalancesmb.com%2Fentrepreneur-simple-business-plan-template-4126711%3Futm_source%3Dlinkedin%26utm_medium%3Dsocial%26utm_campaign%3Dshareurlbuttons"
  network="linkedin" click-tracked="true" title="Share on Linkedin">
  <img src="data:image/svg+xml;base64,PHN2ZyBjbGFzcz0iaWNvbiBpY29uLWxpbmtlZGluICI+Cjx1c2UgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhsaW5rOmhyZWY9IiNpY29uLWxpbmtlZGluIj48L3VzZT4KPC9zdmc+" class="icon icon-linkedin" />
  Share </span>
- <span class="share-link share-link-email"
  data-href="https://www.thebalancesmb.com/entrepreneur-simple-business-plan-template-4126711?utm_source=emailshare&amp;utm_medium=social&amp;utm_campaign=shareurlbuttons"
  network="emailshare" click-tracked="true"
  title="Email this article">
  <img src="data:image/svg+xml;base64,PHN2ZyBjbGFzcz0iaWNvbiBpY29uLWVudmVsb3BlMiAiPgo8dXNlIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4bGluazpocmVmPSIjaWNvbi1lbnZlbG9wZTIiPjwvdXNlPgo8L3N2Zz4=" class="icon icon-envelope2" />
  Email </span>

- <span
  id="masonry-list__item_1-0"><a href="https://www.thebalancesmb.com/a-coffee-shop-business-plan-4153010" id="mntl-card_1-0" class="comp mntl-card mntl-document-card card"></a></span>
  <img src="data:image/gif;charset=utf-8;base64,R0lGODlhCgAHAPYAAAcSEAQZGx8UDQA2PiYVDigaDyMjJi0qLS0rMSwtMjcwLDAyNzQ1OzQ0PTc4PjgxNTs7RDo8QTs9Qj0/RD9BOwBRWABacD5ARj9BRzxBS0A/RF5KP3ZJPkBASUJCTURET0ZOVUxLUU1NWFNLS19hZnJaTmliTXl2jBLM1XyNiZ1rV45nd4BwdKuOb92XdoSIkIyRnZKSn5yZpbGLh6e0vb+mobe3xNWfhMmsk860pujBse3OvtTGz9vK2dLQ1tLT2OrS0vPSy/ne0+Tj6wcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSEAcSECwAAAAACgAHAEYIUQCDmDBAAYaECSQaYHigoceOFDd0oMiBg8WQAwsiXJjxIYOIGAo4qEgwAEABAQR4tHhxwsEFCR0QeJCxwgeQGiGEgIDAYISNHy5oVAhgocSGgAA7" class="lazyload card__img" />

  <span class="card__title"><span class="card__title-text">Here's What Your Coffee Shop
  Business Plan Should Look Like</span></span>

- <span
  id="masonry-list__item_1-0-1"><a href="https://www.thebalancesmb.com/business-plans-the-basics-of-creating-a-business-plan-1794264" id="mntl-card_1-0-1" class="comp mntl-card mntl-document-card card"></a></span>
  <img src="data:image/gif;charset=utf-8;base64,R0lGODlhCAAIAPUAAAICAAICAggEBQgDCQoKCg8PDw8QCxQLAhANCBcuKGBFMm9TPUBfWX1gTmN9ehV3miiwKHmcsohpV5d/XY90YaxJZpmCcKKKZKOOecueA8KpDsqjeIq6iKqYhL+wna6vqa61rrKwsbjIvrHRzKfL4dK3mdnIrOfUtu3TuPLVt8PHyMnMw8zIyczKy83d3NDb1c3g3N3gzdXh3czl4fX18/b38vb29vr6+vv7+wICAAICAAICAAICAAICAAICAAICACwAAAAACAAIAEUISgBr2DjwoAKHFghOGFjBQMQABxRo4GhB8UOLCwFgpCghQ4GEDjRusAihAUKLGDNMjCDQIoOACSBcRCCR4EUDDABQbFBRYIEFDwEBADs=" class="lazyload card__img" alt="Business Plan" />

  <span class="card__title"><span class="card__title-text">How to Write a Business Plan for
  Your Home Business</span></span>

- <span
  id="masonry-list__item_1-0-2"><a href="https://www.thebalancesmb.com/one-page-business-plan-templates-4135972" id="mntl-card_1-0-2" class="comp mntl-card mntl-document-card card"></a></span>
  <img src="data:image/gif;charset=utf-8;base64,R0lGODlhCgAHAPUAAAAAAAEAAAQAAAkAAAsIAAgLBAAFEQEEEwMRGhcDAhYHBBUIABcJBhkBABoMARMRBRATDB4RARMdFQAaKxMrLyMMBCUSASsVACciHz0+OQBFTCxFSjZrfUwvA0g7KmQ2Am4zB208AWg/E1JGLm5BAHVbQnFgTHB0d3JzeAF7klN4iGp3gHeLlIlbHZBTAJxfBJBmKqtnFIJjR597Wb9yWMyGLtODIN2OJe+VAOaWI9uXRP62Urelm4Oww+bm5u/v7ywAAAAACgAHAEUIUABlaKDQAACAAxMCpNhRI8YPHS9AQCjwIMQIEwZpsJhxYkOHCyQiiFgwQEACAAQycChhg4ECACok4MjxwYUHHihW9ADQAsYNHxgQGKhgwUFAADs=" class="lazyload card__img" />

  <span class="card__title"><span class="card__title-text">One-Page Business Plan Templates
  for Entrepreneurs</span></span>

- <span
  id="masonry-list__item_1-0-3"><a href="https://www.thebalancesmb.com/operating-section-of-business-plan-2947031" id="mntl-card_1-0-3" class="comp mntl-card mntl-document-card card"></a></span>
  <img src="data:image/gif;charset=utf-8;base64,R0lGODlhCgAHAPYAACIVEjApEitAUk02JUVNUEpddUxdcldkbllmeGRrclJpgGpzhGl8jnJ/hHWImqV8c4SUm5eZloCTpIOUq4KZtZyho5ymrJiktqurq6Szv6ewvKmzvbG1u5WuyJyvwq65x7a6xLe7xbrCz77Dzb/EzrnC1b3I1LjH5sO8uMbAvcrDv8vEvsbEwsnFwMjFxsnHxc3GwM3Ky8bL1MPL2sjN1s3O0MrP2NLLx9LMy9XPzNfR0NLS2tfW3NHZ6eDb2OHg3uTh4ubl4+Xn6eTq6+nr7/b29iIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiIVEiwAAAAACgAHAEYIUQAx2PiQYQSNEAw6VDhRAAGJBB4sBAAwYAeIEg4WPGhARIAIFylQ3AiiQwWMFS2GbDgQgcMMDQQmQKBgQEGNIiYkCLkgowcOHz+A8IjBIseLgAA7" class="lazyload card__img" alt="Two men planning operations in an empty warehouse" />

  <span class="card__title"><span class="card__title-text">Writing an Operations Plan for
  Your Business</span></span>

- <span
  id="masonry-list__item_1-0-4"><a href="https://www.thebalancesmb.com/how-to-write-the-market-analysis-section-2951562" id="mntl-card_1-0-4" class="comp mntl-card mntl-document-card card"></a></span>
  <img src="data:image/gif;charset=utf-8;base64,R0lGODlhCgAHAPYAAAEBAQQEAgYFAwUFBQcFBggHBQkJCRAPCzMgHCwiICEnMy8rOi84R2tHOVRVUHh9g5VvXJ11a6BzYLJ1eoWFe5CLeKmEcbCPfIGIkIeOlpKXmpKZo56foZ6jpqqqqKy5v7Svq720r7W1s7G1uLO7xr3HycHCvMbKy87Ozs/N0svP2M/R3dvUztfT0NzX0drZ1Nvd3N/f39TY4dbZ4tXZ5dTd5OLe2+Pj4+bm5OHk6ePm7ejp4+rp5O3v7Ojr8urq8uzt8vP07vTx7PPy8Pr79gEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBASwAAAAACgAHAEYIUQABDDAQhAiKEUJ2wHDAoYKFCDmAxDDBgMABCjpmrJgggcYLDBk6nPDg4gaODR8CFEDQgwgJEUN42BCQQIWMGj8uQPDR4oEGEA1CsCiRYoGCgAA7" class="lazyload card__img" alt="Businessman writing the market analysis section of a business plan at his desk." />

  <span class="card__title"><span class="card__title-text">How to Write the Market Analysis
  Section of a Business Plan</span></span>

- <span
  id="masonry-list__item_1-0-5"><a href="https://www.thebalancesmb.com/business-plan-outline-2947032" id="mntl-card_1-0-5" class="comp mntl-card mntl-document-card card"></a></span>
  <img src="data:image/gif;charset=utf-8;base64,R0lGODlhCgAHAPQAACw7Pi48Py5AQExeYtZlXYV/gcKEXZGfn8fDt83HudvUxNrUxtnh4/vb3Pz///7+/v7////+/////yw7Piw7Piw7Piw7Piw7Piw7Piw7Piw7Piw7Piw7Piw7Piw7Piw7PiwAAAAACgAHAEQIOQAbBAjQoEAABwQbIBgwQILDhxIaQHQYQYIBBQkQGNgo4QCBBiAlQgS5gIFDAQ8BGFjo8IGBlhACAgA7" class="lazyload card__img" alt="Business plan outline example: milo&#39;s cupcake factor business plan. Opens to a book index page, sections including: Executive summary, business/industry overview, market analysis, the competition, sales and marketing plan, ownership and management plan. operating plan, financial plan (unknown) and exhibits." />

  <span class="card__title"><span class="card__title-text">The Sections a Winning Business
  Plan Has to Have</span></span>

- <span
  id="masonry-list__item_1-0-6"><a href="https://www.thebalancesmb.com/business-plan-2947267" id="mntl-card_1-0-6" class="comp mntl-card mntl-document-card card"></a></span>
  <img src="data:image/gif;charset=utf-8;base64,R0lGODlhCgAHAPYAAAMDAxIRFxYcHhYbJCIjJiAoLycpLyQpMSI2MjYrIjs1KD8yLD44LD8+OCpJST9DVEY9L0I/RERAOElDN1VMPllLP2NQPX5HPHxMNkxMTEFLWExSVVNUVlVVU1dVW1pdXGFUTWpkWmRkZmd5kH6Ce4ddRqh+S7d0hIqAbb+HWcKBVMqAY8KUb9Sba5KMgZuShpaXkqSflraxpa/Cw8++o8q/rMW+utC/qMDFt8fCu9rPqtTCsdnKuPTAosXKy9zQwd3XzNfX3+fezurf0u3k2vLq3QMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAywAAAAACgAHAEYIUQB5uBAiY0iMIjmA2HigIQOIAwV6cOjw4keLBiQS3KgxwwQRAAF84DCwYYEDDyNoWAgBQQEDChMqoIhwQoeECztYlPggIsWAIAQEwFiBQAWGgAA7" class="lazyload card__img" alt="Executives crafting a business plan" />

  <span class="card__title"><span class="card__title-text">What Is a Business
  Plan?</span></span>

- <span
  id="masonry-list__item_1-0-7"><a href="https://www.thebalancesmb.com/management-section-of-business-plan-2947028" id="mntl-card_1-0-7" class="comp mntl-card mntl-document-card card"></a></span>
  <img src="data:image/gif;charset=utf-8;base64,R0lGODlhCgAHAPUAACMaHSwbFiIeKiwfID0rN0AkHeVxUdOTfLiyrr67pr2/vMO8udK9qNS/rMvAs9PCq9DBr9TCr9fCrcfIzcvLy8fM1c7O0NbQxNTU1tnZ0d/b1NvY2d7f4eDUw+TZzPLTwfPayeDh2+Lh3+Tj3+Tm6Orp5Orp5e7u5erq6urq7O/u6O7s7env8vXv7vHx5/Hy9PHy9/Ty8PT08vb3+fn5+fn5+/n6/Pr7/fv8/vz8/Pz9//39//7+/P7+/v///yMaHSwAAAAACgAHAEUIUABF1PBxg8OEChocMKCQgoQJGSpitBAQAQOMFzQ+2MjQwQcEBCNKrDhxAUCCHi424NCxA8eNGSEUPLDAAkUOED4OeAjQYAEPH0AJDChgQEJAADs=" class="lazyload card__img" alt="Two business women discussing a project." />

  <span class="card__title"><span class="card__title-text">How to Write the Management
  Section of Your Business Plan</span></span>

- <span
  id="masonry-list__item_2-0"><a href="https://www.thebalancesmb.com/write-a-food-truck-business-plan-4066702" id="mntl-card_2-0" class="comp mntl-card mntl-document-card card"></a></span>
  <img src="data:image/gif;charset=utf-8;base64,R0lGODlhCgAHAPYAAAkDCQoCCAsDCgoECg0FCQwFCw4GCg4GCw4UBBEHDBIICxIIDhENDhMLExQMERQVDhgSDhISFBISHhYTGRwXIRMqCR8uCgkkJCkfEzIYBjMaEjYeFS8kGCw4HTogDzEoFzcoGTstHCYkKyksKy4+NTA1KzQ8ITcwNjAuQypZKTJMKjpTKlMtJVc7MkBAH1JLAFFKF1JOHUpQPVNLL09hOHBGOm9lJXBiJHFhLV9lb35qTGBmZ3qIkn+4rXinxIdrRJNsVL6aXP/GcoKPjoaNkNzY0wkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCQkDCSwAAAAACgAHAEYIUQAXSLBxI8YKFTQqWAAgYIIIFEM4EGHhwkCDIC2E+PgBpEOKEiQiMICQI4GJER8GUAARAgOOGToQ9AjggICGDUVkXPCQQcGLAgdq7IDB48SDgAA7" class="lazyload card__img" alt="Group of customers in line at food truck at night" />

  <span class="card__title"><span class="card__title-text">Everything You Need to Write a
  Food Truck Business Plan</span></span>

- <span
  id="masonry-list__item_2-0-1"><a href="https://www.thebalancesmb.com/a-comprehensive-business-plan-outline-for-small-business-2951557" id="mntl-card_2-0-1" class="comp mntl-card mntl-document-card card"></a></span>
  <img src="data:image/gif;charset=utf-8;base64,R0lGODlhCgAHAPYAAA8PDxERERQUFBYWFhcXFxgYGBsbGx4dIiAgICIiIiMjIyUlIyYmJigoKCkpKSsrLSsqLy0tLS0tLy8vLywtMi4uMDcqJDwvKTwxLzExMzIyNDQ0Njc3OTk5Ozs7PT09PT09Pz0+Qk4/PEE8QH1CNEBAQEBAQkJCREdHSUdITE1NT1BQUFFRUVdWUlZWVlhYWmFiZmNjZWVkamZlam9wdHFydnNzdXp6fHd3gX9+hIF7e5mRfIqFi52hqqyegaenqa6usLm6vg8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDywAAAAACgAHAEYIUQAbxAByw4WKEitY8ICwoYIHHSRCIJhhYoSADC8U+EgAowcKAQEAXNjRIoIDBhw6/DDwQMEEGjWC4NBw4oANAxYIUMghY0CCAgtEYEjxAYSEgAA7" class="lazyload card__img" alt="A young male erasing business plans on blackboard" />

  <span class="card__title"><span class="card__title-text">Get Tips on Writing a
  Comprehensive Outline for Your Business Plan</span></span>

- <span
  id="masonry-list__item_2-0-2"><a href="https://www.thebalancesmb.com/prepare-an-investor-ready-business-plan-2947200" id="mntl-card_2-0-2" class="comp mntl-card mntl-document-card card"></a></span>
  <img src="data:image/gif;charset=utf-8;base64,R0lGODlhCgAHAPUAACYmJisrKywqLS0tLTEvMjAwMDExMzIwMTU1NzY2Njg2OTk3ODk5OTk5Ozo4Ozs7Ozw6PT07PD07Pjw8Pj89QD8/QUE/QkJAQ0JCQkZERUdFRkhGR0lHSElJSUpKSktLS0pKTE1LTExMTE1NTU9NTk5OTk9NUFBQUGVdSHtrSXJna3FxcXR0dIJ1S4t1To6Ojry8vLO0xsa4tdOoodC1qty/t8HBwd/IwNbW1tra2u7x9iYmJiYmJiYmJiYmJiYmJiwAAAAACgAHAEUIUQAPLHjwIMKECQ0UELCx4cOHGjc6dNhA4cWHEiVIjAjhgYMFHAYABIihosAAGAJWYNCAooWLFBksQMgh4sQJGjNKmABxwUEFFgl0yGBAQQKCgAA7" class="lazyload card__img" alt="Person standing at a blackboard with business plan strategies written around their head" />

  <span class="card__title"><span class="card__title-text">How to Prepare an Investor-Ready
  Business Plan</span></span>

- <span
  id="masonry-list__item_2-0-3"><a href="https://www.thebalancesmb.com/create-a-startup-business-plan-in-easy-steps-397548" id="mntl-card_2-0-3" class="comp mntl-card mntl-document-card card"></a></span>
  <img src="data:image/gif;charset=utf-8;base64,R0lGODlhCQAHAPUAABoZFyAkJzgxK4qGepqWi7WZi6aklamjl6yonbCtnq+spbavpbquosG9tMXCs8TAt8XAusbEuMjGusrGvcrIvMzIv8PDwczHwc7LxM7LxsjIyM/Pz9XVzdTU1NfX19jY1tvb293d3d7e3t/f393e4N7g3+Dg4OHh4eLi4uHi5Ofn5/Ly8vPz8/Hy9PT09PT29fX39vf39/j4+Pr6+vv7+xoZFxoZFxoZFxoZFxoZFxoZFxoZFxoZFxoZFxoZFxoZFywAAAAACQAHAEUISAARKDAwAMCBBBMyPBhRYgSICBYIFIjBYkWHEC5gyDhBAsUIGiAxVODAooOHFhtcvGCxAIIEEQ4uNPigIoCJFCdm0KDAQIOAgAA7" class="lazyload card__img" alt="Start-up group creating a business plan portfolio by laying papers on the floor.to determine the flow." />

  <span class="card__title"><span class="card__title-text">Create Your Startup Business
  Plan</span></span>

- <span
  id="masonry-list__item_2-0-4"><a href="https://www.thebalancesmb.com/writing-the-business-plan-section-8-2947026" id="mntl-card_2-0-4" class="comp mntl-card mntl-document-card card"></a></span>
  <img src="data:image/gif;charset=utf-8;base64,R0lGODlhCgAHAPYAABYWDB8gGCgnIigoIC4uJjMzKzY3KTgxKzs8Lj9AOkE6J0hJO0tMPlJNOW9MLmFQNHVUK3dWKXxTP1BPVVNSTV1TSlxZRl1bTF5gUmRiVWxzSnNvVmhqdoJePoVlPopsRqF8R4WEcImIdIqLeZGOb5mUbpOSfpOWhZWXgqWgjKOknra0qNKzsdLHmc7GocvJutTLpNTMp9XKrNnOrtrSruDZvMLCwNDOwtXSydXRzuDb1ePi3fPt1eno5Ozt6Pb18/T09vj4+Pn5+fj4+hYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDBYWDCwAAAAACgAHAEYIUQBN2GgBY0YFBTxkBBmRoUEPIAQQFEhAAcWEACtesHBwA4eOECl+SPAAIcICDRtU5DgQg8aDDy5qCDlxYcCOIB0EcMAQRISPIRZAADDAgESJgAA7" class="lazyload card__img" alt="Businessman working on financial plan at his computer" />

  <span class="card__title"><span class="card__title-text">Business Plan Essentials: Writing
  a Cash Flow Projection</span></span>

- <span
  id="masonry-list__item_2-0-5"><a href="https://www.thebalancesmb.com/how-to-start-a-home-based-business-that-will-succeed-2948192" id="mntl-card_2-0-5" class="comp mntl-card mntl-document-card card"></a></span>
  <img src="data:image/gif;charset=utf-8;base64,R0lGODlhCgAHAPYAABAXHwgaLgkaNggeNg0fNSQtMhImRxoyWDA+WE85O1hcXUBTZEdWbVlgcHBycT5lqGt1gW2AkYdkTpFyXqdxWbpxRKuQe6+Qe7CRdYOOipSKgJSOgJ2ZjZGZm5afqJmZpZSgoJmjopuouKuTiaKrurO3uLu9uLi+vrm/v660wL/BvLbCzrvBwbnDxbjG0czGtuPNpMHHx8LGx8XHxsLHysvByc7Kwc7Qy9DMw9nb2Nne2Nrc2dnd3Nrg3Nvm7OLr6Orv6+rz8vj58/7+9P/++RAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHxAXHywAAAAACgAHAEYIUQAzeOgwg4WMGDyEgHCxYsMAATAaDCEhAoWOHQko/DBxg4gDDQsIWGBw4UMNH0Fa0AhxAkAPFTmIKCgBZISBBxUKSMAwAYeNAwEQvEgBIQKHgAA7" class="lazyload card__img" alt="Getting her home business up and running" />

  <span class="card__title"><span class="card__title-text">6 Steps to Starting a Home-Based
  Business That Will Succeed</span></span>

- <span
  id="masonry-list__item_2-0-6"><a href="https://www.thebalancesmb.com/best-business-plan-books-2890050" id="mntl-card_2-0-6" class="comp mntl-card mntl-document-card card"></a></span>
  <img src="data:image/gif;charset=utf-8;base64,R0lGODlhCgAHAPYAAAExOwI0PR1MPBBTQSlKUzFJUzBnSFtiWnFST2hpV391XHNuanFxb3Jxb3B6fHyQdIF7W4p4dpeOhZ2Ui5KSko6Yor6tm7+0nsCskcGwoM2wqsq4ts/Dt9HAttLAvtTMud7Et9nFut3Ls9nMu93QvcfHz87FyNTGw9PMxtbNxtzJwtbSz9/Tw93QytDR1uDRyuDWzeTWyeXXyubZyOXZzebc0Orc0eje1Ord1ene3Ovg2u/m4e3o7u3o7/Hs8vTt9fTs9/Xu9vft9vju9/rw+Pvw+AExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOwExOywAAAAACgAHAEYIUQAz7BBggEaEAiE0bMCgAEKAH0GIIODQIkaNCwQoJDiAQkIOFh9GPGCwoEGKFSBk3BgAwEQJFzA8WMBhQ0QFHj18OHihYwaJCUWGABFyQkWHgAA7" class="lazyload card__img" alt="Business Plan on Note Pad" />

  <span class="card__title"><span class="card__title-text">The 9 Best Business Plan Books of
  2021</span></span>

- <span
  id="masonry-list__item_2-0-7"><a href="https://www.thebalancesmb.com/how-to-write-a-home-business-plan-1794222" id="mntl-card_2-0-7" class="comp mntl-card mntl-document-card card"></a></span>
  <img src="data:image/gif;charset=utf-8;base64,R0lGODlhCgAHAPYAAAkSDQERExAXFSMbFCAjFiErJjAzMlE4I0NRMlNlf3l2ZXt7coJ4W5+Wf4qvVo6KhYqNiKCLk7esoLqvocm0ntW6nsO5qcW7q8C+usW+uNC/r87EtM7Eut7HrNTJvsvLydfJwNHO09TQyd7Uzt7Yz9LS0NnY3eTa0Ofe1uHc2eHe3+bi2ebi3ejj2uni3P7mz+jj4Orl4u/r5vXt4Pbt4vHv8Prw5vrz7fj07/n18Pn38/j29P328Pr49vv59/z48fz48/v4+QkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDQkSDSwAAAAACgAHAEYIUQB3TOixggOBGDJalLDRQQOPEQUUYEgh4cYFGjpAHIigggUJIEEoVEBhIACDBQ58WJiRwwOAGhBcnLiBY8ONBA9gfEAw4MWPBjtECEhhIkSGgAA7" class="lazyload card__img" alt="Woman writing business plan in home business office" />

  <span class="card__title"><span class="card__title-text">How to Write a Home Business
  Plan</span></span>

<img src="data:image/svg+xml;base64,PHN2ZyBjbGFzcz0iaWNvbiBsb2dvX190ZXh0ICI+Cjx1c2UgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhsaW5rOmhyZWY9IiNsb2dvX190ZXh0Ij48L3VzZT4KPC9zdmc+" class="icon logo__text" />

<img src="data:image/svg+xml;base64,PHN2ZyBjbGFzcz0iaWNvbiAzLXVwLS1iIGZvb3Rlcl9fc29jaWFsLWF2YXRhciI+Cjx1c2UgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhsaW5rOmhyZWY9IiMzLXVwLS1iIj48L3VzZT4KPC9zdmc+" class="icon 3-up--b footer__social-avatar" />

Follow Us

<a href="https://www.facebook.com/thebalancecom/" class="social-follow__icon social-follow__facebook"><img src="data:image/svg+xml;base64,PHN2ZyBjbGFzcz0iaWNvbiBpY29uLWZhY2Vib29rIGljb24gaWNvbi1mYWNlYm9vayI+Cjx1c2UgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhsaW5rOmhyZWY9IiNpY29uLWZhY2Vib29rIj48L3VzZT4KPC9zdmc+" class="icon icon-facebook icon icon-facebook" />
<span class="social-follow__icon-text">Facebook</span></a>
<a href="https://twitter.com/thebalance/" class="social-follow__icon social-follow__twitter"><img src="data:image/svg+xml;base64,PHN2ZyBjbGFzcz0iaWNvbiBpY29uLXR3aXR0ZXIgaWNvbiBpY29uLXR3aXR0ZXIiPgo8dXNlIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4bGluazpocmVmPSIjaWNvbi10d2l0dGVyIj48L3VzZT4KPC9zdmc+" class="icon icon-twitter icon icon-twitter" />
<span class="social-follow__icon-text">Twitter</span></a>
<a href="https://www.instagram.com/thebalancecom/" class="social-follow__icon social-follow__instagram"><img src="data:image/svg+xml;base64,PHN2ZyBjbGFzcz0iaWNvbiBpY29uLWluc3RhZ3JhbSBpY29uIGljb24taW5zdGFncmFtIj4KPHVzZSB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeGxpbms6aHJlZj0iI2ljb24taW5zdGFncmFtIj48L3VzZT4KPC9zdmc+" class="icon icon-instagram icon icon-instagram" />
<span class="social-follow__icon-text">Instagram</span></a>
<a href="https://www.linkedin.com/company/the-balance.com/" class="social-follow__icon social-follow__linkedin"><img src="data:image/svg+xml;base64,PHN2ZyBjbGFzcz0iaWNvbiBpY29uLWxpbmtlZGluIGljb24gaWNvbi1saW5rZWRpbiI+Cjx1c2UgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhsaW5rOmhyZWY9IiNpY29uLWxpbmtlZGluIj48L3VzZT4KPC9zdmc+" class="icon icon-linkedin icon icon-linkedin" />
<span class="social-follow__icon-text">Linkedin</span></a>

- <a href="https://www.thebalancesmb.com/about-us" class="footer-links__link">About Us</a>
- <a href="https://www.dotdash.com/our-brands/" class="footer-links__link">Advertise</a>
- <a href="https://www.thebalancesmb.com/about-us#EditorialGuidelines" class="footer-links__link">Editorial
  Guidelines</a>
- <a href="http://jobs.jobvite.com/dotdash" class="footer-links__link">Careers</a>
- <a href="https://www.thebalancesmb.com/about-us#ContactUs" class="footer-links__link">Contact</a>
- <a href="/terms-of-use-and-policies-the-balance-smb-4847378#the-balance-cookie-disclosure" class="footer-links__link">Cookie
  Policy</a>
- <a href="/terms-of-use-and-policies-the-balance-smb-4847378#terms-of-use" class="footer-links__link">Terms
  of Use</a>
- <a href="/terms-of-use-and-policies-the-balance-smb-4847378#privacy-policy" class="footer-links__link">Privacy
  Policy</a>
- <a href="/terms-of-use-and-policies-the-balance-smb-4847378#jurisdiction-specific-provisions" class="footer-links__link">California
  Privacy Notice</a>

Also From The Balance Team

<a href="https://www.thebalance.com/" id="logo_2-0" class="comp family-nav__item logo mntl-block"><img src="data:image/svg+xml;base64,PHN2ZyBjbGFzcz0iaWNvbiBmYW1pbHktbmF2LW1vbmV5IGZhbWlseS1uYXZfX2xvZ28taWNvbi0tbW9uZXkgZmFtaWx5LW5hdl9fbG9nby1pY29uIj4KPHVzZSB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeGxpbms6aHJlZj0iI2ZhbWlseS1uYXYtbW9uZXkiPjwvdXNlPgo8L3N2Zz4=" class="icon family-nav-money family-nav__logo-icon--money family-nav__logo-icon" />
The Balance</a>
<a href="https://www.thebalancecareers.com/" id="logo_4-0" class="comp family-nav__item logo mntl-block"><img src="data:image/svg+xml;base64,PHN2ZyBjbGFzcz0iaWNvbiBmYW1pbHktbmF2LWNhcmVlcnMgZmFtaWx5LW5hdl9fbG9nby1pY29uLS1jYXJlZXJzIGZhbWlseS1uYXZfX2xvZ28taWNvbiI+Cjx1c2UgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhsaW5rOmhyZWY9IiNmYW1pbHktbmF2LWNhcmVlcnMiPjwvdXNlPgo8L3N2Zz4=" class="icon family-nav-careers family-nav__logo-icon--careers family-nav__logo-icon" />
The Balance: Careers</a>

<img src="data:image/svg+xml;base64,PHN2ZyBjbGFzcz0iaWNvbiBtbnRsLWRvdGRhc2gtZmFtaWx5LW5hdl9fbG9nbyAiPgo8dXNlIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4bGluazpocmVmPSIjbW50bC1kb3RkYXNoLWZhbWlseS1uYXZfX2xvZ28iPjwvdXNlPgo8L3N2Zz4=" class="icon mntl-dotdash-family-nav__logo" />
<span class="mntl-dotdash-family-nav__title"> The Balance Small Business
is part of the
<a href="https://www.dotdash.com" class="mntl-dotdash-family-nav__title--link">Dotdash</a>
publishing family. </span>

<img src="data:image/svg+xml;base64,PHN2ZyBjbGFzcz0iaXMtaGlkZGVuIj4KPGRlZnM+CjxzeW1ib2wgaWQ9Im1udGwtc2MtYmxvY2stc3RhcnJhdGluZy1pY29uIj4KPHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdib3g9IjAgMCAyMCAyMCI+CjxwYXRoIGQ9Ik0wLDB2MjBoMjBWMEgweiBNMTQuMiwxMi4ybDEuMSw2LjNsLTUuNC0zLjJsLTUuMSwzLjJsMS02LjNMMS40LDhsNS45LTAuOGwyLjYtNS44bDIuNyw1LjhMMTguNSw4TDE0LjIsMTIuMnoiPjwvcGF0aD4KPC9zdmc+" class="is-hidden" />
