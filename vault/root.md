---
id: root
title: Watheia Knowledgespace
desc: Organizational Knowledgespace for Watheia Labs
updated: 1619372231472
created: 1595961348801
stub: false
---

The Watheia Labs Knowledgespace is a catalog of documents, facts, and figures that define our domain.

## Active Projects

- [watheia/mfe-harmonics](https://gitlab.com/watheia/mfe-harmonics)
- [watheia/mfe-api](https://gitlab.com/watheia/mfe-api
- [watheia/mfe-home](https://gitlab.com/watheia/mfe-home))
- [watheia/mfe-dashboard](https://gitlab.com/watheia/mfe-dashboard))
- [watheia/mfe-modelserver](https://gitlab.com/watheia/mfe-modelserver))
- [watheia/screenplay](https://gitlab.com/watheia/mfe-screenplay))