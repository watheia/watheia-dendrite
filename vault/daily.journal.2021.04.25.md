---
id: b1e9559c-bce3-4a57-be04-0779b0d3cfd8
title: '2021-04-25'
desc: ''
updated: 1619379061881
created: 1619378707212
---

**Mission**: Launch the MVP of our landing page

## TODO

- [ ] Content Updates
  - [ ] Home
    - [ ] Makers
    - [ ] Designers
    - [ ] Developers
  - [ ] About
  - [ ] Contact
- [ ] Contact Service API
- [ ] Tech Demo
- [ ] Cloud-Native Environment

